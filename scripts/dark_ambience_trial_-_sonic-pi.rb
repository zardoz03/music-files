use_debug false
use_synth :dark_ambience
puts'you have now entered the catacombs'

live_loop :hell_cathedral do
  play rrand(20,40), amp: 0.25, decay: 5
  sleep 0.75
  sample :ambi_choir, amp: 1.5, decay: 10, attack_level: 20, rate: 0.25
  sleep 0.75
end