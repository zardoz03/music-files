####-Track 1-#####


###-Description and Other Declarations-##

#u | connolly
use_debug false

##~FUNCTIONS~##

define :set1 do |a,p|
  use_synth :dsaw
  play rrand(50,60), rate: rrand(0.1,12), amp: a, pan: p, decay: 10 , attack: 7
  sleep 0.1
end

define :set2 do |a,p|
  use_synth :prophet
  play rrand(12,13),rate: rrand(0.1,10), amp: 2*a , pan: -(p), decay: 10 , attack: 7
end

define :backing do
  use_synth :dark_ambience
  play rrand(20,40), amp: 0.25, decay: 6
  sleep 0.75
  sample :ambi_choir, amp: 1.5, decay: 10, attack_level: 20, rate: 0.25
  sleep 0.75
end

##~~MAIN~~##

puts 'what if /mu/ was a file on your computer'
in_thread do
  loop do
    cue :tick
    sleep 0.1
  end
end

#tick timer (above)

in_thread do
  sync :tick
  with_fx :bitcrusher , sample_rate:(1000), bits:(5) do
    live_loop :back do
      5.times do
        backing
      end
    end
  end
end

sleep 20

in_thread do
  sync :tick
  live_loop :foo do
    set1 6.5,0
    sleep 0.2
    set2 6.5,0
    sleep 0.2
  end
end

sleep 34
in_thread do
  sync :tick
  live_loop :foo1 do
    set2 (7*(((rrand(rrand(1,3),(rrand(1,3))))))),(rrand(-1,1))
    sleep 0.1
    set1 (7*(1)),(0)
    sleep 0.1
    set2 (7*(rrand(1,3))),(rrand(-1,1))
    sleep 0.1
  end
end