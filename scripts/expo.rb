use_debug false

q  = 0

e  = 2.7182818284590452353602874713526624977572470936999595749669676277240766303535475945713821785251664274
pi = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679

use_synth :square
define :h do |k|
  play k ** q, rate: 4
end

live_loop :tick do
  cue :tick
  sleep 1
end

with_fx :bitcrusher, sample_rate: 1000 do
  with_fx :compressor do
    in_thread do
      sync :tick
      live_loop :expo do
        q += 0.001
        h e
        sleep 0.1
        puts q
      end
    end
  end
end