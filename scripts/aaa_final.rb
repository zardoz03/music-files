use_debug false
use_cue_logging false

##emd var is a mispelling of end so weird shit wouldnt happen
q = 0
live_loop :tack do
  cue :tick
  sleep 1
  puts q
  q += 1
end

define :fack do |a,d,p,emd|
  use_synth :dsaw
  play 30, amp: a, decay: d, pan: p
  play 34, amp: a, decay: d, pan: p
  sleep 0.4
  play 24, amp: 0.5*a, decay: d, pan: p
  play 23, amp: 0.5*a, decay: d, pan: p
  sleep rrand(emd,4*emd)
end

define :feick do |a,d,p,emd|
  use_synth :prophet
  play 54, amp: 0.5*a, decay: d, pan: p
  play 53, amp: 0.5*a, decay: d, pan: p
  sleep 0.2
  play 10, amp: a, decay: d, pan: p
  play 14, amp: a, decay: d, pan: p
  sleep rrand(emd,4*emd)
end

loop do
  cue :tick
  fack 0.5, 10, rrand(-1,1),0.2
  sleep 10
  feick 1, 10, rrand(-1,1),0.4
end